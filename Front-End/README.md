# ProjetWeb-Master-2-IWOCS
On cherche à développer une application Web avec des composants graphiques avancés capables d’afficher les données relatives aux ventes immobilières en France.


## Members

- Armand GUELINA
- Diakarou SOKHONA
- Ibrahima SOW
- Mamel Alboury NDIAYE

# Front-end

- downgrade to node 16
- run npm outdated
- update packagejson file with the required dependencies
- import properly mouse and d3.mouse delete d3
- for airbnb :
	sudo npm install -g install-peerdeps
	install-peerdeps --dev eslint-config-airbnb
	install-peerdeps --dev eslint-config-airbnb-base

Launch : ```npm run start```
