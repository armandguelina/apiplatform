# ApiPlatform

# ProjetWeb-Master-2-IWOCS
On cherche à développer une application Web avec des composants graphiques avancés capables d’afficher les données relatives aux ventes immobilières en France.

## Members

- Armand GUELINA
- Diakarou SOKHONA
- Ibrahima SOW
- Mamel Alboury NDIAYE

## installation

1. Cloner le projet

2. Back-End server
	- cd ProjetWeb-Master-2-IWOCS/Back-End/
	- read README.md file
	
3. 2. Front-End server
	- cd ProjetWeb-Master-2-IWOCS/Front-End/
	- read README.md file

