<?php

/**
 * This file has been auto-generated
 * by the Symfony Routing Component.
 */

return [
    false, // $matchHost
    [ // $staticRoutes
        '/api/land/linechart' => [[['_route' => 'api_land_values_get_line_chart_collection', '_controller' => 'App\\Controller\\Api\\LineChartController', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\LandValue', '_api_identifiers' => [], '_api_has_composite_identifier' => false, '_api_collection_operation_name' => 'get_line_chart'], null, ['GET' => 0], null, false, false, null]],
        '/api/land/donutchart' => [[['_route' => 'api_land_values_get_donut_chart_collection', '_controller' => 'App\\Controller\\Api\\DonutChartController', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\LandValue', '_api_identifiers' => [], '_api_has_composite_identifier' => false, '_api_collection_operation_name' => 'get_donut_chart'], null, ['GET' => 0], null, false, false, null]],
        '/api/land/barchart' => [[['_route' => 'api_land_values_get_bar_chart_collection', '_controller' => 'App\\Controller\\Api\\BarChartController', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\LandValue', '_api_identifiers' => [], '_api_has_composite_identifier' => false, '_api_collection_operation_name' => 'get_bar_chart'], null, ['GET' => 0], null, false, false, null]],
    ],
    [ // $regexpList
        0 => '{^(?'
                .'|/api(?'
                    .'|(?:/(index)(?:\\.([^/]++))?)?(*:42)'
                    .'|/(?'
                        .'|docs(?:\\.([^/]++))?(*:72)'
                        .'|contexts/(.+)(?:\\.([^/]++))?(*:107)'
                    .')'
                .')'
                .'|/_error/(\\d+)(?:\\.([^/]++))?(*:145)'
            .')/?$}sDu',
    ],
    [ // $dynamicRoutes
        42 => [[['_route' => 'api_entrypoint', '_controller' => 'api_platform.action.entrypoint', '_format' => '', '_api_respond' => 'true', 'index' => 'index'], ['index', '_format'], null, null, false, true, null]],
        72 => [[['_route' => 'api_doc', '_controller' => 'api_platform.action.documentation', '_format' => '', '_api_respond' => 'true'], ['_format'], null, null, false, true, null]],
        107 => [[['_route' => 'api_jsonld_context', '_controller' => 'api_platform.jsonld.action.context', '_format' => 'jsonld', '_api_respond' => 'true'], ['shortName', '_format'], null, null, false, true, null]],
        145 => [
            [['_route' => '_preview_error', '_controller' => 'error_controller::preview', '_format' => 'html'], ['code', '_format'], null, null, false, true, null],
            [null, null, null, null, false, false, 0],
        ],
    ],
    null, // $checkCondition
];
