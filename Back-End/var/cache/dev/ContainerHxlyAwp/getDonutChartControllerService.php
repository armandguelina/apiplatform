<?php

namespace ContainerHxlyAwp;

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;

/**
 * @internal This class has been auto-generated by the Symfony Dependency Injection Component.
 */
class getDonutChartControllerService extends App_KernelDevDebugContainer
{
    /**
     * Gets the public 'App\Controller\Api\DonutChartController' shared autowired service.
     *
     * @return \App\Controller\Api\DonutChartController
     */
    public static function do($container, $lazyLoad = true)
    {
        include_once \dirname(__DIR__, 4).'/vendor/symfony/framework-bundle/Controller/AbstractController.php';
        include_once \dirname(__DIR__, 4).'/src/Controller/Api/DonutChartController.php';

        $container->services['App\\Controller\\Api\\DonutChartController'] = $instance = new \App\Controller\Api\DonutChartController(($container->privates['App\\Repository\\LandValueRepository'] ?? $container->load('getLandValueRepositoryService')), ($container->services['request_stack'] ?? ($container->services['request_stack'] = new \Symfony\Component\HttpFoundation\RequestStack())));

        $instance->setContainer(($container->privates['.service_locator.mx0UMmY'] ?? $container->load('get_ServiceLocator_Mx0UMmYService'))->withContext('App\\Controller\\Api\\DonutChartController', $container));

        return $instance;
    }
}
