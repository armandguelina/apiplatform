<?php

namespace ContainerHxlyAwp;
include_once \dirname(__DIR__, 4).'/vendor/doctrine/persistence/lib/Doctrine/Persistence/ObjectManager.php';
include_once \dirname(__DIR__, 4).'/vendor/doctrine/orm/lib/Doctrine/ORM/EntityManagerInterface.php';
include_once \dirname(__DIR__, 4).'/vendor/doctrine/orm/lib/Doctrine/ORM/EntityManager.php';

class EntityManager_9a5be93 extends \Doctrine\ORM\EntityManager implements \ProxyManager\Proxy\VirtualProxyInterface
{
    /**
     * @var \Doctrine\ORM\EntityManager|null wrapped object, if the proxy is initialized
     */
    private $valueHolder723b9 = null;

    /**
     * @var \Closure|null initializer responsible for generating the wrapped object
     */
    private $initializeree612 = null;

    /**
     * @var bool[] map of public properties of the parent class
     */
    private static $publicProperties474f5 = [
        
    ];

    public function getConnection()
    {
        $this->initializeree612 && ($this->initializeree612->__invoke($valueHolder723b9, $this, 'getConnection', array(), $this->initializeree612) || 1) && $this->valueHolder723b9 = $valueHolder723b9;

        return $this->valueHolder723b9->getConnection();
    }

    public function getMetadataFactory()
    {
        $this->initializeree612 && ($this->initializeree612->__invoke($valueHolder723b9, $this, 'getMetadataFactory', array(), $this->initializeree612) || 1) && $this->valueHolder723b9 = $valueHolder723b9;

        return $this->valueHolder723b9->getMetadataFactory();
    }

    public function getExpressionBuilder()
    {
        $this->initializeree612 && ($this->initializeree612->__invoke($valueHolder723b9, $this, 'getExpressionBuilder', array(), $this->initializeree612) || 1) && $this->valueHolder723b9 = $valueHolder723b9;

        return $this->valueHolder723b9->getExpressionBuilder();
    }

    public function beginTransaction()
    {
        $this->initializeree612 && ($this->initializeree612->__invoke($valueHolder723b9, $this, 'beginTransaction', array(), $this->initializeree612) || 1) && $this->valueHolder723b9 = $valueHolder723b9;

        return $this->valueHolder723b9->beginTransaction();
    }

    public function getCache()
    {
        $this->initializeree612 && ($this->initializeree612->__invoke($valueHolder723b9, $this, 'getCache', array(), $this->initializeree612) || 1) && $this->valueHolder723b9 = $valueHolder723b9;

        return $this->valueHolder723b9->getCache();
    }

    public function transactional($func)
    {
        $this->initializeree612 && ($this->initializeree612->__invoke($valueHolder723b9, $this, 'transactional', array('func' => $func), $this->initializeree612) || 1) && $this->valueHolder723b9 = $valueHolder723b9;

        return $this->valueHolder723b9->transactional($func);
    }

    public function wrapInTransaction(callable $func)
    {
        $this->initializeree612 && ($this->initializeree612->__invoke($valueHolder723b9, $this, 'wrapInTransaction', array('func' => $func), $this->initializeree612) || 1) && $this->valueHolder723b9 = $valueHolder723b9;

        return $this->valueHolder723b9->wrapInTransaction($func);
    }

    public function commit()
    {
        $this->initializeree612 && ($this->initializeree612->__invoke($valueHolder723b9, $this, 'commit', array(), $this->initializeree612) || 1) && $this->valueHolder723b9 = $valueHolder723b9;

        return $this->valueHolder723b9->commit();
    }

    public function rollback()
    {
        $this->initializeree612 && ($this->initializeree612->__invoke($valueHolder723b9, $this, 'rollback', array(), $this->initializeree612) || 1) && $this->valueHolder723b9 = $valueHolder723b9;

        return $this->valueHolder723b9->rollback();
    }

    public function getClassMetadata($className)
    {
        $this->initializeree612 && ($this->initializeree612->__invoke($valueHolder723b9, $this, 'getClassMetadata', array('className' => $className), $this->initializeree612) || 1) && $this->valueHolder723b9 = $valueHolder723b9;

        return $this->valueHolder723b9->getClassMetadata($className);
    }

    public function createQuery($dql = '')
    {
        $this->initializeree612 && ($this->initializeree612->__invoke($valueHolder723b9, $this, 'createQuery', array('dql' => $dql), $this->initializeree612) || 1) && $this->valueHolder723b9 = $valueHolder723b9;

        return $this->valueHolder723b9->createQuery($dql);
    }

    public function createNamedQuery($name)
    {
        $this->initializeree612 && ($this->initializeree612->__invoke($valueHolder723b9, $this, 'createNamedQuery', array('name' => $name), $this->initializeree612) || 1) && $this->valueHolder723b9 = $valueHolder723b9;

        return $this->valueHolder723b9->createNamedQuery($name);
    }

    public function createNativeQuery($sql, \Doctrine\ORM\Query\ResultSetMapping $rsm)
    {
        $this->initializeree612 && ($this->initializeree612->__invoke($valueHolder723b9, $this, 'createNativeQuery', array('sql' => $sql, 'rsm' => $rsm), $this->initializeree612) || 1) && $this->valueHolder723b9 = $valueHolder723b9;

        return $this->valueHolder723b9->createNativeQuery($sql, $rsm);
    }

    public function createNamedNativeQuery($name)
    {
        $this->initializeree612 && ($this->initializeree612->__invoke($valueHolder723b9, $this, 'createNamedNativeQuery', array('name' => $name), $this->initializeree612) || 1) && $this->valueHolder723b9 = $valueHolder723b9;

        return $this->valueHolder723b9->createNamedNativeQuery($name);
    }

    public function createQueryBuilder()
    {
        $this->initializeree612 && ($this->initializeree612->__invoke($valueHolder723b9, $this, 'createQueryBuilder', array(), $this->initializeree612) || 1) && $this->valueHolder723b9 = $valueHolder723b9;

        return $this->valueHolder723b9->createQueryBuilder();
    }

    public function flush($entity = null)
    {
        $this->initializeree612 && ($this->initializeree612->__invoke($valueHolder723b9, $this, 'flush', array('entity' => $entity), $this->initializeree612) || 1) && $this->valueHolder723b9 = $valueHolder723b9;

        return $this->valueHolder723b9->flush($entity);
    }

    public function find($className, $id, $lockMode = null, $lockVersion = null)
    {
        $this->initializeree612 && ($this->initializeree612->__invoke($valueHolder723b9, $this, 'find', array('className' => $className, 'id' => $id, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializeree612) || 1) && $this->valueHolder723b9 = $valueHolder723b9;

        return $this->valueHolder723b9->find($className, $id, $lockMode, $lockVersion);
    }

    public function getReference($entityName, $id)
    {
        $this->initializeree612 && ($this->initializeree612->__invoke($valueHolder723b9, $this, 'getReference', array('entityName' => $entityName, 'id' => $id), $this->initializeree612) || 1) && $this->valueHolder723b9 = $valueHolder723b9;

        return $this->valueHolder723b9->getReference($entityName, $id);
    }

    public function getPartialReference($entityName, $identifier)
    {
        $this->initializeree612 && ($this->initializeree612->__invoke($valueHolder723b9, $this, 'getPartialReference', array('entityName' => $entityName, 'identifier' => $identifier), $this->initializeree612) || 1) && $this->valueHolder723b9 = $valueHolder723b9;

        return $this->valueHolder723b9->getPartialReference($entityName, $identifier);
    }

    public function clear($entityName = null)
    {
        $this->initializeree612 && ($this->initializeree612->__invoke($valueHolder723b9, $this, 'clear', array('entityName' => $entityName), $this->initializeree612) || 1) && $this->valueHolder723b9 = $valueHolder723b9;

        return $this->valueHolder723b9->clear($entityName);
    }

    public function close()
    {
        $this->initializeree612 && ($this->initializeree612->__invoke($valueHolder723b9, $this, 'close', array(), $this->initializeree612) || 1) && $this->valueHolder723b9 = $valueHolder723b9;

        return $this->valueHolder723b9->close();
    }

    public function persist($entity)
    {
        $this->initializeree612 && ($this->initializeree612->__invoke($valueHolder723b9, $this, 'persist', array('entity' => $entity), $this->initializeree612) || 1) && $this->valueHolder723b9 = $valueHolder723b9;

        return $this->valueHolder723b9->persist($entity);
    }

    public function remove($entity)
    {
        $this->initializeree612 && ($this->initializeree612->__invoke($valueHolder723b9, $this, 'remove', array('entity' => $entity), $this->initializeree612) || 1) && $this->valueHolder723b9 = $valueHolder723b9;

        return $this->valueHolder723b9->remove($entity);
    }

    public function refresh($entity)
    {
        $this->initializeree612 && ($this->initializeree612->__invoke($valueHolder723b9, $this, 'refresh', array('entity' => $entity), $this->initializeree612) || 1) && $this->valueHolder723b9 = $valueHolder723b9;

        return $this->valueHolder723b9->refresh($entity);
    }

    public function detach($entity)
    {
        $this->initializeree612 && ($this->initializeree612->__invoke($valueHolder723b9, $this, 'detach', array('entity' => $entity), $this->initializeree612) || 1) && $this->valueHolder723b9 = $valueHolder723b9;

        return $this->valueHolder723b9->detach($entity);
    }

    public function merge($entity)
    {
        $this->initializeree612 && ($this->initializeree612->__invoke($valueHolder723b9, $this, 'merge', array('entity' => $entity), $this->initializeree612) || 1) && $this->valueHolder723b9 = $valueHolder723b9;

        return $this->valueHolder723b9->merge($entity);
    }

    public function copy($entity, $deep = false)
    {
        $this->initializeree612 && ($this->initializeree612->__invoke($valueHolder723b9, $this, 'copy', array('entity' => $entity, 'deep' => $deep), $this->initializeree612) || 1) && $this->valueHolder723b9 = $valueHolder723b9;

        return $this->valueHolder723b9->copy($entity, $deep);
    }

    public function lock($entity, $lockMode, $lockVersion = null)
    {
        $this->initializeree612 && ($this->initializeree612->__invoke($valueHolder723b9, $this, 'lock', array('entity' => $entity, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializeree612) || 1) && $this->valueHolder723b9 = $valueHolder723b9;

        return $this->valueHolder723b9->lock($entity, $lockMode, $lockVersion);
    }

    public function getRepository($entityName)
    {
        $this->initializeree612 && ($this->initializeree612->__invoke($valueHolder723b9, $this, 'getRepository', array('entityName' => $entityName), $this->initializeree612) || 1) && $this->valueHolder723b9 = $valueHolder723b9;

        return $this->valueHolder723b9->getRepository($entityName);
    }

    public function contains($entity)
    {
        $this->initializeree612 && ($this->initializeree612->__invoke($valueHolder723b9, $this, 'contains', array('entity' => $entity), $this->initializeree612) || 1) && $this->valueHolder723b9 = $valueHolder723b9;

        return $this->valueHolder723b9->contains($entity);
    }

    public function getEventManager()
    {
        $this->initializeree612 && ($this->initializeree612->__invoke($valueHolder723b9, $this, 'getEventManager', array(), $this->initializeree612) || 1) && $this->valueHolder723b9 = $valueHolder723b9;

        return $this->valueHolder723b9->getEventManager();
    }

    public function getConfiguration()
    {
        $this->initializeree612 && ($this->initializeree612->__invoke($valueHolder723b9, $this, 'getConfiguration', array(), $this->initializeree612) || 1) && $this->valueHolder723b9 = $valueHolder723b9;

        return $this->valueHolder723b9->getConfiguration();
    }

    public function isOpen()
    {
        $this->initializeree612 && ($this->initializeree612->__invoke($valueHolder723b9, $this, 'isOpen', array(), $this->initializeree612) || 1) && $this->valueHolder723b9 = $valueHolder723b9;

        return $this->valueHolder723b9->isOpen();
    }

    public function getUnitOfWork()
    {
        $this->initializeree612 && ($this->initializeree612->__invoke($valueHolder723b9, $this, 'getUnitOfWork', array(), $this->initializeree612) || 1) && $this->valueHolder723b9 = $valueHolder723b9;

        return $this->valueHolder723b9->getUnitOfWork();
    }

    public function getHydrator($hydrationMode)
    {
        $this->initializeree612 && ($this->initializeree612->__invoke($valueHolder723b9, $this, 'getHydrator', array('hydrationMode' => $hydrationMode), $this->initializeree612) || 1) && $this->valueHolder723b9 = $valueHolder723b9;

        return $this->valueHolder723b9->getHydrator($hydrationMode);
    }

    public function newHydrator($hydrationMode)
    {
        $this->initializeree612 && ($this->initializeree612->__invoke($valueHolder723b9, $this, 'newHydrator', array('hydrationMode' => $hydrationMode), $this->initializeree612) || 1) && $this->valueHolder723b9 = $valueHolder723b9;

        return $this->valueHolder723b9->newHydrator($hydrationMode);
    }

    public function getProxyFactory()
    {
        $this->initializeree612 && ($this->initializeree612->__invoke($valueHolder723b9, $this, 'getProxyFactory', array(), $this->initializeree612) || 1) && $this->valueHolder723b9 = $valueHolder723b9;

        return $this->valueHolder723b9->getProxyFactory();
    }

    public function initializeObject($obj)
    {
        $this->initializeree612 && ($this->initializeree612->__invoke($valueHolder723b9, $this, 'initializeObject', array('obj' => $obj), $this->initializeree612) || 1) && $this->valueHolder723b9 = $valueHolder723b9;

        return $this->valueHolder723b9->initializeObject($obj);
    }

    public function getFilters()
    {
        $this->initializeree612 && ($this->initializeree612->__invoke($valueHolder723b9, $this, 'getFilters', array(), $this->initializeree612) || 1) && $this->valueHolder723b9 = $valueHolder723b9;

        return $this->valueHolder723b9->getFilters();
    }

    public function isFiltersStateClean()
    {
        $this->initializeree612 && ($this->initializeree612->__invoke($valueHolder723b9, $this, 'isFiltersStateClean', array(), $this->initializeree612) || 1) && $this->valueHolder723b9 = $valueHolder723b9;

        return $this->valueHolder723b9->isFiltersStateClean();
    }

    public function hasFilters()
    {
        $this->initializeree612 && ($this->initializeree612->__invoke($valueHolder723b9, $this, 'hasFilters', array(), $this->initializeree612) || 1) && $this->valueHolder723b9 = $valueHolder723b9;

        return $this->valueHolder723b9->hasFilters();
    }

    /**
     * Constructor for lazy initialization
     *
     * @param \Closure|null $initializer
     */
    public static function staticProxyConstructor($initializer)
    {
        static $reflection;

        $reflection = $reflection ?? new \ReflectionClass(__CLASS__);
        $instance   = $reflection->newInstanceWithoutConstructor();

        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $instance, 'Doctrine\\ORM\\EntityManager')->__invoke($instance);

        $instance->initializeree612 = $initializer;

        return $instance;
    }

    protected function __construct(\Doctrine\DBAL\Connection $conn, \Doctrine\ORM\Configuration $config, \Doctrine\Common\EventManager $eventManager)
    {
        static $reflection;

        if (! $this->valueHolder723b9) {
            $reflection = $reflection ?? new \ReflectionClass('Doctrine\\ORM\\EntityManager');
            $this->valueHolder723b9 = $reflection->newInstanceWithoutConstructor();
        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $this, 'Doctrine\\ORM\\EntityManager')->__invoke($this);

        }

        $this->valueHolder723b9->__construct($conn, $config, $eventManager);
    }

    public function & __get($name)
    {
        $this->initializeree612 && ($this->initializeree612->__invoke($valueHolder723b9, $this, '__get', ['name' => $name], $this->initializeree612) || 1) && $this->valueHolder723b9 = $valueHolder723b9;

        if (isset(self::$publicProperties474f5[$name])) {
            return $this->valueHolder723b9->$name;
        }

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder723b9;

            $backtrace = debug_backtrace(false, 1);
            trigger_error(
                sprintf(
                    'Undefined property: %s::$%s in %s on line %s',
                    $realInstanceReflection->getName(),
                    $name,
                    $backtrace[0]['file'],
                    $backtrace[0]['line']
                ),
                \E_USER_NOTICE
            );
            return $targetObject->$name;
        }

        $targetObject = $this->valueHolder723b9;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __set($name, $value)
    {
        $this->initializeree612 && ($this->initializeree612->__invoke($valueHolder723b9, $this, '__set', array('name' => $name, 'value' => $value), $this->initializeree612) || 1) && $this->valueHolder723b9 = $valueHolder723b9;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder723b9;

            $targetObject->$name = $value;

            return $targetObject->$name;
        }

        $targetObject = $this->valueHolder723b9;
        $accessor = function & () use ($targetObject, $name, $value) {
            $targetObject->$name = $value;

            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __isset($name)
    {
        $this->initializeree612 && ($this->initializeree612->__invoke($valueHolder723b9, $this, '__isset', array('name' => $name), $this->initializeree612) || 1) && $this->valueHolder723b9 = $valueHolder723b9;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder723b9;

            return isset($targetObject->$name);
        }

        $targetObject = $this->valueHolder723b9;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();

        return $returnValue;
    }

    public function __unset($name)
    {
        $this->initializeree612 && ($this->initializeree612->__invoke($valueHolder723b9, $this, '__unset', array('name' => $name), $this->initializeree612) || 1) && $this->valueHolder723b9 = $valueHolder723b9;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder723b9;

            unset($targetObject->$name);

            return;
        }

        $targetObject = $this->valueHolder723b9;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);

            return;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $accessor();
    }

    public function __clone()
    {
        $this->initializeree612 && ($this->initializeree612->__invoke($valueHolder723b9, $this, '__clone', array(), $this->initializeree612) || 1) && $this->valueHolder723b9 = $valueHolder723b9;

        $this->valueHolder723b9 = clone $this->valueHolder723b9;
    }

    public function __sleep()
    {
        $this->initializeree612 && ($this->initializeree612->__invoke($valueHolder723b9, $this, '__sleep', array(), $this->initializeree612) || 1) && $this->valueHolder723b9 = $valueHolder723b9;

        return array('valueHolder723b9');
    }

    public function __wakeup()
    {
        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $this, 'Doctrine\\ORM\\EntityManager')->__invoke($this);
    }

    public function setProxyInitializer(\Closure $initializer = null) : void
    {
        $this->initializeree612 = $initializer;
    }

    public function getProxyInitializer() : ?\Closure
    {
        return $this->initializeree612;
    }

    public function initializeProxy() : bool
    {
        return $this->initializeree612 && ($this->initializeree612->__invoke($valueHolder723b9, $this, 'initializeProxy', array(), $this->initializeree612) || 1) && $this->valueHolder723b9 = $valueHolder723b9;
    }

    public function isProxyInitialized() : bool
    {
        return null !== $this->valueHolder723b9;
    }

    public function getWrappedValueHolderValue()
    {
        return $this->valueHolder723b9;
    }
}

if (!\class_exists('EntityManager_9a5be93', false)) {
    \class_alias(__NAMESPACE__.'\\EntityManager_9a5be93', 'EntityManager_9a5be93', false);
}
