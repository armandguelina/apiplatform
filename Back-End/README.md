# ProjetWeb-Master-2-IWOCS
On cherche à développer une application Web avec des composants graphiques avancés capables d’afficher les données relatives aux ventes immobilières en France.

## Members

- Armand GUELINA
- Diakarou SOKHONA
- Ibrahima SOW
- Mamel Alboury NDIAYE


# Back end

- composer install
- php bin/console doctrine:database:create
- php bin/console make:migration
- php bin/console doctrine:migrations:migrate
- mkdir data
- Add files valeursfoncieres-2017.txt, valeursfoncieres-2018.txt, valeursfoncieres-2019.txt, valeursfoncieres-2020.txt, valeursfoncieres-2021.txt, valeursfoncieres-2022.txt into data
- php bin/console app:landValue:populate
- A data.sql is created. Fill the database with it
-
### Swagger
localhost:8000/api

### Error 
Memory errors. Upgrade limit in php.ini

```memory_limit = 1024M```

Reload server
